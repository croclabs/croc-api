## How to use

Add to your repositories definition:

```gradle
maven {
    name "Croclabs"
    url "https://gitlab.com/api/v4/groups/13802051/-/packages/maven"
}
```

In your dependencies add:

```gradle
implementation 'croclabs:croc-api:0.0.1'
```
