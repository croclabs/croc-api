package croclabs.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Interface for a create HTTP controller.
 * 
 * @author croclabs
 *
 * @param <B> The request body type
 */
public interface ICreate<B> {
	/**
	 * Create method for Spring HTTP. Make sure to add {@link PostMapping} and {@link ResponseBody}.
	 * 
	 * @param <T> The return type, might be {@link Void}
	 * @param body The request body
	 * @return A {@link ResponseEntity} object holding information about the response
	 */
	@PostMapping
	@ResponseBody
	<T> ResponseEntity<T> create(@RequestBody B body);
}
