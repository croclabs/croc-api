package croclabs.api;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import croclabs.api.annotations.Param;

/**
 * Abstract Service class used to autowire {@link HttpServletRequest}, {@link HttpServletResponse} and {@link HttpSession}.
 * These values are autowired thread safe, meaning they can be used in a singleton context without problems.
 * <br><br>
 * This class provides helpful functionality like getting parameters, attributes and similar things from the request/session
 * and can be used to return {@link ResponseEntity} by calling the respective methods.
 * <br><br>
 * Make sure to annotate your service with {@link Service}
 * 
 * @author croclabs
 *
 */
public abstract class AbstractService {
	protected @Autowired HttpServletRequest request;
	protected @Autowired HttpServletResponse response;
	protected @Autowired HttpSession session;

	public AbstractService() {
		super();
	}

	/**
	 * Retrieves a parameter from the request.
	 * 
	 * @param name The name of the parameter
	 * @return The parameter value as String
	 */
	protected String parameter(String name) {
		return request.getParameter(name);
	}
	
	/**
	 * Retrieves a parameter from the request and type casts it by given class.
	 * This method calls another method annotated with {@link Param} that uses
	 * the given class as value.
	 * 
	 * @param <T> The type of the class to cast to
	 * @param name The name of the parameter
	 * @param clazz The class object to cast to
	 * @return The parameter casted to given type, or null if there was no respectively annotated method provided
	 */
	@SuppressWarnings("unchecked")
	protected <T> T parameter(String name, Class<T> clazz) {
		String param = this.parameter(name);
		
		for (Method m : this.getClass().getDeclaredMethods()) {
			if (m.isAnnotationPresent(Param.class) &&
					m.getDeclaredAnnotation(Param.class).value().equals(clazz)) {
				try {
					return (T) m.invoke(this, param);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		
		return null;
	}
	
	protected <T> ResponseEntity<T> ok() {
		return ResponseEntity.ok().build();
	}
	
	protected <T> ResponseEntity<T> ok(T body) {
		return ResponseEntity.ok(body);
	}
	
	protected <T> ResponseEntity<T> created(URI uri) {
		return ResponseEntity.created(uri).build();
	}
	
	protected <T> ResponseEntity<T> created(URI uri, T body) {
		return ResponseEntity.created(uri).body(body);
	}
	
	protected <T> ResponseEntity<T> accepted() {
		return ResponseEntity.accepted().build();
	}
	
	protected <T> ResponseEntity<T> accepted(T body) {
		return ResponseEntity.accepted().body(body);
	}
	
	protected <T> ResponseEntity<T> badRequest() {
		return ResponseEntity.badRequest().build();
	}
	
	protected <T> ResponseEntity<T> badRequest(T body) {
		return ResponseEntity.badRequest().body(body);
	}
	
	protected <T> ResponseEntity<T> notFound() {
		return ResponseEntity.notFound().build();
	}
	
	protected <T> ResponseEntity<T> noContent() {
		return ResponseEntity.noContent().build();
	}
	
	protected <T> ResponseEntity<T> internalServerError() {
		return ResponseEntity.internalServerError().build();
	}
	
	protected <T> ResponseEntity<T> internalServerError(T body) {
		return ResponseEntity.internalServerError().body(body);
	}
	
	protected <T> ResponseEntity<T> unprocessableEntity() {
		return ResponseEntity.unprocessableEntity().build();
	}
	
	protected <T> ResponseEntity<T> unprocessableEntity(T body) {
		return ResponseEntity.unprocessableEntity().body(body);
	}
	
	protected <T> ResponseEntity<T> status(HttpStatus status) {
		return ResponseEntity.status(status).build();
	}
	
	protected <T> ResponseEntity<T> status(int status) {
		return ResponseEntity.status(status).build();
	}
	
	protected <T> ResponseEntity<T> status(HttpStatus status, T body) {
		return ResponseEntity.status(status).body(body);
	}
	
	protected <T> ResponseEntity<T> status(int status, T body) {
		return ResponseEntity.status(status).body(body);
	}
}
