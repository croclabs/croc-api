package croclabs.api;

/**
 * Identifier interface that extends {@link ICreate}, {@link IRead}, {@link IUpdate} and {@link IDelete}.
 * 
 * @author croclabs
 *
 * @param <B> The request body object type used for create and update. If not 
 * specifically defined or different for update and create use {@link Object} 
 * or implement the interfaces on the controller all by themselves.
 */
public interface ICrud<B> extends ICreate<B>, IRead, IUpdate<B>, IDelete {

}
