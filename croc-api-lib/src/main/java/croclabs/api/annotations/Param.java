package croclabs.api.annotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import croclabs.api.AbstractService;

/**
 * Use this annotation to define a method for the method invocation in {@link AbstractService#parameter(String, Class)}.
 * Each class value should only be used once per service.
 * 
 * @author croclabs
 *
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface Param {
	/**
	 * Class to type cast to
	 * @return the suggested class object
	 */
	Class<?> value();
}
