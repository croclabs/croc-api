package croclabs.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Interface for a update HTTP controller.
 * 
 * @author croclabs
 *
 * @param <B>
 */
public interface IUpdate<B> {
	/**
	 * Update method for Spring HTTP. Make sure to add {@link PutMapping} and {@link ResponseBody}.
	 * 
	 * @param <T> The return type, might be {@link Void}
	 * @param body The request body
	 * @return A {@link ResponseEntity} object holding information about the response
	 */
	@PutMapping
	@ResponseBody
	<T> ResponseEntity<T> update(@RequestBody B body);
}
