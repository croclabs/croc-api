package croclabs.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * Extend this controller to build a spring MVC controller. This class
 * autowires the given service class so you have full access over this
 * field inside your controller.
 * <br><br>
 * Make sure to add {@link Controller} as an annotation to your controller.
 * You can also add {@link Scope} to determine the context of your controller.
 * Default is singleton.
 * 
 * @author croclabs
 *
 * @param <T> The service class, needs to extend {@link AbstractService}.
 */
public abstract class AbstractController<T extends AbstractService> {
	protected @Autowired T service;
	
	public AbstractController() {
		super();
	}

}
