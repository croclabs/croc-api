package croclabs.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Interface for a read HTTP controller.
 * 
 * @author croclabs
 *
 */
public interface IRead {
	/**
	 * Create method for Spring HTTP. Make sure to add {@link GetMapping} and {@link ResponseBody}.
	 * 
	 * @param <T> The return type, might be {@link Void}
	 * @return A {@link ResponseEntity} object holding information about the response
	 */
	@GetMapping
	@ResponseBody
	<T> ResponseEntity<T> read();
}
