package croclabs.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Interface for a delete HTTP controller.
 * 
 * @author croclabs
 *
 */
public interface IDelete {
	/**
	 * Create method for Spring HTTP. Make sure to add {@link DeleteMapping} and {@link ResponseBody}.
	 * 
	 * @param <T> The return type, might be {@link Void}
	 * @return A {@link ResponseEntity} object holding information about the response
	 */
	@DeleteMapping
	@ResponseBody
	<T> ResponseEntity<T> delete();
}
