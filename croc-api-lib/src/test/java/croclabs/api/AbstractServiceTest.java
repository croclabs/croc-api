package croclabs.api;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;

class AbstractServiceTest {
	static ServiceImpl service = new ServiceImpl();
	
	@BeforeAll
	static void setup() {
		MockHttpServletRequest requestMock = new MockHttpServletRequest();
		requestMock.addParameter("test1", "val1");
		requestMock.addParameter("testLong", "12345");
		
		service.request = requestMock;
	}

	@Test
	@DisplayName("testService")
	void test() {
		Assertions.assertEquals("val1", service.parameter("test1"));
		Assertions.assertEquals(12345L, service.parameter("testLong", Long.class));
	}

}
