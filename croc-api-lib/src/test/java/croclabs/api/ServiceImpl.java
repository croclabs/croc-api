package croclabs.api;

import croclabs.api.annotations.Param;

public class ServiceImpl extends AbstractService {
	@Param(Long.class)
	protected Long paramToLong(String param) {
		return Long.parseLong(param);
	}
}
